package com.example.marius.homewor;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by NgocTri on 11/15/2015.
 */
public class ProductListAdapter extends BaseAdapter {
    int[] IMAGES = {R.drawable.dota, R.drawable.csgo, R.drawable.gta, R.drawable.tomb};

    private Context mContext;
    private List<Product> mProductList;

    //Constructor

    public ProductListAdapter(Context mContext, List<Product> mProductList) {
        this.mContext = mContext;
        this.mProductList = mProductList;
    }

    @Override
    public int getCount() {
        return mProductList.size();
    }

    @Override
    public Object getItem(int position) {
        return mProductList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = View.inflate(mContext, R.layout.item_product_list, null);
        TextView objName = (TextView)v.findViewById(R.id.obj_name);
        TextView objPrice = (TextView)v.findViewById(R.id.obj_price);
        TextView objDescription = (TextView)v.findViewById(R.id.obj_description);
        ImageView objImage = (ImageView)v.findViewById(R.id.obj_image) ;
        //Set text for TextView
        objName.setText(mProductList.get(position).getName());
        objPrice.setText(String.valueOf(mProductList.get(position).getPrice()) + " $");
        objDescription.setText(mProductList.get(position).getDescription());
        objImage.setImageResource(IMAGES[position]);

        //Save product id to tag
        v.setTag(mProductList.get(position).getId());

        return v;
    }
}
